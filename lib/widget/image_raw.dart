import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:online_shop_ku/util/resources.dart';

class ImageRaw extends StatelessWidget {
  String url;
  ImageWidgetBuilder imageBuilder;

  ImageRaw({this.url, this.imageBuilder});
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      fit: BoxFit.cover,
      imageUrl: url ??
          "https://dummyimage.com/500x500/ffffff/000000&text=Image+not+Available",
      placeholder: (context, url) => new Center(
          child: CircularProgressIndicator(
        backgroundColor: ResColor.primary,
      )),
      errorWidget: (context, url, error) =>
          new Center(child: Container(child: Icon(Icons.error))),
      imageBuilder: imageBuilder ?? null,
    );
  }
}
