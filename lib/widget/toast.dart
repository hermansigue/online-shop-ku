import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ToastHelper {
  static Function toast({String msg}) {
    Fluttertoast.showToast(
        msg: msg ?? "",
        toastLength: Toast.LENGTH_LONG,
        backgroundColor: Colors.black87);
  }
}
