import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:online_shop_ku/model/item_model.dart';
import 'package:online_shop_ku/util/helper.dart';
import 'package:online_shop_ku/util/network.dart';
import 'package:online_shop_ku/util/resources.dart';

import 'image_raw.dart';

class ItemTile extends StatelessWidget {
  Item item;
  Function ontap;
  Function callback;
  ItemTile({this.item, this.ontap, this.callback});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(0),
      child: InkWell(
        splashColor: ResColor.primary,
        onTap: () {
          print("Item");
          ontap();
        },
        child: Card(
          semanticContainer: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          clipBehavior: Clip.antiAlias,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  child: Stack(children: [
                ImageRaw(
                  url: "${item.image}",
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider, fit: BoxFit.cover),
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: InkWell(
                            splashColor: ResColor.primary,
                            highlightColor: Colors.transparent,
                            onTap: () async {
                              print("Favorite");
                              print(
                                  '${Helper.getHostApi()}item/favorite/${item.id}');
                              await Dio()
                                  .put(
                                      '${Helper.getHostApi()}item/favorite/${item.id}')
                                  .then((response) {
                                print(response);
                                if (response.data['status'] == 'ok') {}
                              }).catchError((error) {
                                NetworkHelper.dioError(
                                    context: context, error: error);
                              });

                              if (callback != null) callback();
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: ResColor.white,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Icon(
                                  (item.favorite)
                                      ? CupertinoIcons.heart_fill
                                      : CupertinoIcons.heart,
                                  size: 30,
                                  color: ResColor.primary,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                )
              ])),
              Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(children: [
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: Text(item.name,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 16)),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        if (item.discount_price < item.price)
                          Text(
                            "\$ ${item.price}",
                            style: TextStyle(
                                color: Colors.grey[400],
                                fontWeight: FontWeight.w600,
                                decoration: TextDecoration.lineThrough),
                          ),
                        Text(
                          "\$ ${item.discount_price}",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 16),
                        )
                      ],
                    )
                  ])),
            ],
          ),
        ),
      ),
    );
  }
}
