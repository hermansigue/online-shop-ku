import 'package:flutter/material.dart';
import 'package:online_shop_ku/util/resources.dart';

class ErrorConnectionScreen extends StatelessWidget {
  Function func;

  ErrorConnectionScreen({this.func});
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.wifi_off_rounded,
            size: MediaQuery.of(context).size.width * 0.3,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            "Koneksi Bermasalah",
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
          ),
          SizedBox(
            height: 15,
          ),
          RaisedButton(
            color: ResColor.secondary,
            onPressed: () {
              func();
            },
            child: Container(
              color: Colors.transparent,
              width: MediaQuery.of(context).size.width * 0.3,
              child: Expanded(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.replay_outlined,
                    color: Colors.white,
                  ),
                  Text(
                    "Reload",
                    style: TextStyle(color: Colors.white),
                  )
                ],
              )),
            ),
          )
        ],
      ),
    );
  }
}
