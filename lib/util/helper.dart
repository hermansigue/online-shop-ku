import 'package:online_shop_ku/util/resources.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Helper {
  static String getHostApi() {
    return ResString.host;
  }

  static String getHost() {
    return ResString.base;
  }

  static Future<SharedPreferences> initPrefs() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('cCart')) prefs.setInt('cCart', 0);
    print('LOG : Shared pref initiated');
    return prefs;
  }
}
