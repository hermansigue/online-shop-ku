import 'package:flutter/material.dart';

class ResString {
  /**For Local Server */
  // static const String base = "http://192.168.1.4/support-shopping-ku/";
  /** For Dev Server */
  static const String base = "http://213.190.4.209/ssku/";

  static const String host = base + "api/";
}

class ResColor {
  static const Color primary = Color.fromRGBO(249, 129, 131, 1);
  static const Color secondary = Color.fromRGBO(247, 223, 236, 1);
  static const Color white = Color.fromRGBO(254, 245, 250, 1);
}

class ResInt {
  static const animDuration = 300;
  static const cupertinoPageTransitionDuration = 250;
}
