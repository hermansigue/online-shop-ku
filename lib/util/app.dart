import 'package:online_shop_ku/main.dart';

class App {
  static Future<void> setCart(int value) async {
    await mainPrefs.setInt('cCart', value);
  }

  static getCart() {
    return mainPrefs.getInt('cCart');
  }
}
