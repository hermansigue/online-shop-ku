import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class NetworkHelper {
  static BaseOptions dioOption() {
    return BaseOptions(
      connectTimeout: 30000,
      receiveTimeout: 30000,
    );
  }

  static String dioError(
      {@required BuildContext context, @required DioError error}) {
    String msg = '';
    try {
      DioError _error = error;
      if (_error.response == null) {
        msg = 'Tidak dapat terhubung ke server, mohon cek koneksi';
        Fluttertoast.showToast(
            msg: msg,
            toastLength: Toast.LENGTH_LONG,
            backgroundColor: Colors.black87);
      } else {
        var err = jsonDecode(_error.response.toString());
        msg = jsonDecode(_error.response.toString())['message'];

        Fluttertoast.showToast(
            msg: msg,
            toastLength: Toast.LENGTH_LONG,
            backgroundColor: Colors.black87);

        switch (err['status']) {
          case 'error':
            {}
            break;
        }
      }
    } catch (err) {
      print(err);
      msg = err.toString();
      Fluttertoast.showToast(
          msg: msg,
          toastLength: Toast.LENGTH_LONG,
          backgroundColor: Colors.black87);
    }

    return msg;
  }
}
