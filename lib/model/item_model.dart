class Item {
  int id;
  String name;
  double rating;
  int rating_count;
  String description;
  double price;
  double discount_price;
  bool favorite;
  String image;
  Item(
      {this.id,
      this.favorite,
      this.name,
      this.image,
      this.rating,
      this.rating_count,
      this.description,
      this.discount_price,
      this.price});
}
