import 'package:online_shop_ku/model/item_model.dart';

class Cart {
  int id;
  Item item;
  int qty;
  double price;
  double total;
  String color;
  String size;

  Cart({
    this.id,
    this.item,
    this.qty,
    this.price,
    this.total,
    this.color,
    this.size,
  });
}
