import 'package:flutter/material.dart';
import 'package:online_shop_ku/screen/screen_dashboard.dart';
import 'package:online_shop_ku/util/helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();
SharedPreferences mainPrefs;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPreferences.getInstance().then((value) {
    mainPrefs = value;
  });
  await Helper.initPrefs().then((value) async {
    mainPrefs = value;
  });
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Online Shop Ku',
      theme: ThemeData(visualDensity: VisualDensity.adaptivePlatformDensity),
      home: ScreenDashboard(),
      navigatorObservers: [routeObserver],
    );
  }
}
