import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:online_shop_ku/model/cart_model.dart';
import 'package:online_shop_ku/model/item_model.dart';
import 'package:online_shop_ku/util/helper.dart';
import 'package:online_shop_ku/util/network.dart';
import 'package:online_shop_ku/util/resources.dart';
import 'package:online_shop_ku/widget/image_raw.dart';
import 'package:online_shop_ku/widget/loading.dart';

class ScreenCart extends StatefulWidget {
  @override
  _ScreenCartState createState() => _ScreenCartState();
}

class _ScreenCartState extends State<ScreenCart> {
  List<Cart> _list_cart = [];
  bool _isLoad = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getCart();
  }

  @override
  Widget build(BuildContext context) {
    print("Build: ScreenCart");
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.light,
          systemNavigationBarColor: ResColor.primary),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ResColor.secondary,
          shadowColor: Colors.transparent,
          leading: Padding(
            padding: EdgeInsets.all(8),
            child: RawMaterialButton(
              elevation: 0,
              onPressed: () {
                Navigator.of(context).pop();
              },
              fillColor: ResColor.secondary,
              child: Icon(
                Icons.arrow_back_ios,
                size: 25.0,
                color: Colors.black54,
              ),
              padding: EdgeInsets.all(0.0),
            ),
          ),
        ),
        body: Material(
          color: ResColor.secondary,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    _title(
                      title: "Cart",
                    )
                  ],
                ),
              ),
              Expanded(
                child: _isLoad
                    ? LoadingScreen()
                    : _list_cart.length == 0
                        ? Center(
                            child: Text("No data Cart"),
                          )
                        : GridView.builder(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 20.0 / 10.0,
                              crossAxisCount: 1,
                            ),
                            itemCount: _list_cart.length,
                            // itemCount: 3,
                            itemBuilder: (BuildContext context, int index) {
                              return _cardCart(_list_cart[index]);
                            },
                          ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _cardCart(Cart cart) {
    return Padding(
      padding: EdgeInsets.all(0),
      child: Card(
        elevation: 3,
        color: ResColor.primary,
        child: Row(
          children: [
            Flexible(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: ImageRaw(
                  url: "${cart.item.image}",
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider, fit: BoxFit.cover),
                    ),
                  ),
                ),
              ),
            ),
            Flexible(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 15, 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text("Name:",
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w500)),
                      Text(
                        "${cart.item.name}",
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text("Qty:",
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w500)),
                      Text(
                        "${cart.qty} Pcs",
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text("Total:",
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w500)),
                      Text(
                        "\$ ${cart.total.toStringAsFixed(2)}",
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }

  Widget _title({String title}) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Text(
        title ?? "",
        style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
      ),
    ]);
  }

  Future<void> _getCart() async {
    print("Get Cart");
    setState(() {
      _isLoad = true;
    });
    await Dio().get('${Helper.getHostApi()}cart').then((response) async {
      if (response.data['status'] == 'ok') {
        var list_cart = response.data['data'];
        print(list_cart);
        for (var i = 0; i < list_cart.length; i++) {
          _list_cart.add(Cart(
              id: list_cart[i]['id'],
              color: list_cart[i]['color'],
              size: list_cart[i]['size'],
              price: list_cart[i]['price'].toDouble(),
              qty: list_cart[i]['qty'],
              total: list_cart[i]['total'].toDouble(),
              item: Item(
                name: list_cart[i]['item']['name'],
                image: list_cart[i]['item']['image'],
              )));
        }
      }
      await Future.delayed(Duration(seconds: 1));
      setState(() {
        _isLoad = false;
      });
    }).catchError((error) {
      NetworkHelper.dioError(context: context, error: error);
    });
  }
}
