import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:online_shop_ku/model/category_model.dart';
import 'package:online_shop_ku/model/item_model.dart';
import 'package:online_shop_ku/screen/screen_item_detail.dart';
import 'package:online_shop_ku/util/helper.dart';
import 'package:online_shop_ku/util/network.dart';
import 'package:online_shop_ku/util/resources.dart';
import 'package:online_shop_ku/widget/image_raw.dart';
import 'package:online_shop_ku/widget/loading.dart';
import 'package:online_shop_ku/widget/toast.dart';

class ScreenDashboardHome extends StatefulWidget {
  @override
  _ScreenDashboardHomeState createState() => _ScreenDashboardHomeState();
}

class _ScreenDashboardHomeState extends State<ScreenDashboardHome> {
  bool _categoryLoad = false;
  bool _fashionLoad = false;
  int _category_id;
  List<Category> _list_category = [];
  List<Item> _list_item = [];

  @override
  void initState() {
    super.initState();

    _getCategory();

    _getItem();
  }

  @override
  Widget build(BuildContext context) {
    print("Build: ScreenDashboardHome");
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.light,
          systemNavigationBarColor: ResColor.primary),
      child: Material(
        color: ResColor.secondary,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            _header(),
            Padding(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _title(
                      title: "Category",
                      nextPage: () {
                        print("See all category");
                      })
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.only(left: 20, right: 20),
              clipBehavior: Clip.none,
              child: (_categoryLoad)
                  ? LoadingScreen()
                  : Row(
                      children: [
                        for (var i in _list_category)
                          _menuTile(
                              title: "${i.name}",
                              link: "${i.icon}",
                              ontap: () async {
                                _category_id = i.id;
                                print(_category_id);
                                await _getItem();
                              })
                      ],
                    ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _title(
                      title: "Popular Fashion",
                      nextPage: () {
                        print("See all popular fashion");
                      })
                ],
              ),
            ),
            Expanded(
              child: (_fashionLoad)
                  ? LoadingScreen()
                  : GridView.builder(
                      padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        childAspectRatio: 7.0 / 10.0,
                        crossAxisCount: 2,
                      ),
                      // itemCount: 4,
                      itemCount: _list_item.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _itemTile(
                            item: _list_item[index],
                            ontap: () {
                              Navigator.of(context).push(CupertinoPageRoute(
                                  builder: (BuildContext context) {
                                return ScreenItemDetail(
                                  item: _list_item[index],
                                );
                              })).then((value) {
                                _getItem();
                              });
                            });
                      }),
            ),
          ],
        ),
      ),
    );
  }

  Widget _title({String title, Function nextPage}) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Text(
        title ?? "",
        style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
      ),
      InkWell(
        onTap: () {
          // print("See all popular fashion");
          nextPage();
        },
        child: Text(
          "See All",
          style: TextStyle(
              color: ResColor.primary,
              fontWeight: FontWeight.w600,
              fontSize: 14),
        ),
      )
    ]);
  }

  Widget _itemTile({Item item, Function ontap}) {
    return Padding(
      padding: EdgeInsets.all(0),
      child: InkWell(
        splashColor: ResColor.primary,
        onTap: () {
          print("Item");
          ontap();
        },
        child: Card(
          semanticContainer: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          clipBehavior: Clip.antiAlias,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  child: Stack(children: [
                ImageRaw(
                  url: "${item.image} ",
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider, fit: BoxFit.cover),
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: InkWell(
                            splashColor: ResColor.primary,
                            highlightColor: Colors.transparent,
                            onTap: () async {
                              print("Favorite");
                              await _addFavorite(item: item);
                              if (item.favorite) {
                                ToastHelper.toast(
                                    msg: "Item Removed from favorite");
                              } else {
                                ToastHelper.toast(
                                    msg: "Item added to favorite");
                              }
                              item.favorite = !item.favorite;
                              setState(() {});
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: ResColor.white,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Icon(
                                  (item.favorite)
                                      ? CupertinoIcons.heart_fill
                                      : CupertinoIcons.heart,
                                  size: 30,
                                  color: ResColor.primary,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                )
              ])),
              Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(children: [
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: Text(item.name,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 16)),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        if (item.discount_price < item.price)
                          Text(
                            "\$ ${item.price}",
                            style: TextStyle(
                                color: Colors.grey[400],
                                fontWeight: FontWeight.w600,
                                decoration: TextDecoration.lineThrough),
                          ),
                        Text(
                          "\$ ${item.discount_price}",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 16),
                        )
                      ],
                    )
                  ])),
            ],
          ),
        ),
      ),
    );
  }

  Widget _menuTile({String title, @required String link, Function ontap}) {
    return SizedBox(
      width: 115,
      height: 115,
      child: Card(
        color: Colors.white,
        elevation: 0,
        child: InkWell(
          splashColor: ResColor.secondary,
          highlightColor: Colors.transparent,
          onTap: () {
            print("category");
            ontap();
          },
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                  ImageRaw(
                    url: link,
                    imageBuilder: (context, imageProvider) => Container(
                      width: 35.0,
                      height: 35.0,
                      decoration: BoxDecoration(
                        // shape: BoxShape.circle,
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    title ?? "",
                    style: TextStyle(fontWeight: FontWeight.w700),
                  )
                ])),
          ),
        ),
      ),
    );
  }

  Widget _header() {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            "Fashion Shop",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            "Get popular fashion from home",
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
          ),
          SizedBox(
            height: 15,
          ),
          TextField(
            decoration: InputDecoration(
                focusColor: ResColor.white,
                contentPadding: EdgeInsets.all(5),
                prefixIcon: Icon(Icons.search),
                hintText: "Search the clothes you need",
                hintStyle: new TextStyle(color: Colors.grey[400]),
                border: new OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
                fillColor: Colors.white70),
          )
        ],
      ),
    );
  }

  Future<void> _getCategory() async {
    print("Get Category");
    setState(() {
      _categoryLoad = true;
    });
    await Dio().get('${Helper.getHostApi()}category').then((response) async {
      if (response.data['status'] == 'ok') {
        var list_category = response.data['data'];
        for (var i = 0; i < list_category.length; i++) {
          _list_category.add(Category(
              id: list_category[i]['id'],
              name: list_category[i]['name'],
              icon: list_category[i]['icon']));
        }
      }
      await Future.delayed(Duration(seconds: 1));
      setState(() {
        _categoryLoad = false;
      });
    }).catchError((error) {
      NetworkHelper.dioError(context: context, error: error);
    });
  }

  Future<void> _addFavorite({Item item}) async {
    print("Wishlist");
    print('${Helper.getHostApi()}item/favorite/${item.id}');
    await Dio()
        .put('${Helper.getHostApi()}item/favorite/${item.id}')
        .then((response) {
      print(response);
      if (response.data['status'] == 'ok') {}
    }).catchError((error) {
      NetworkHelper.dioError(context: context, error: error);
    });
  }

  Future<void> _getItem() async {
    print("Get Item");
    setState(() {
      _fashionLoad = true;
    });
    print(_category_id);
    await Dio().get('${Helper.getHostApi()}item', queryParameters: {
      'category_id': _category_id ?? null
    }).then((response) async {
      if (response.data['status'] == 'ok') {
        _list_item.clear();
        var list_item = response.data['data'];
        print(list_item[0]);
        for (var i = 0; i < list_item.length; i++) {
          _list_item.add(Item(
              id: list_item[i]['id'],
              name: list_item[i]['name'],
              description: list_item[i]['description'],
              rating: list_item[i]['rating'].toDouble(),
              rating_count: list_item[i]['rating_count'].toInt(),
              price: list_item[i]['price'].toDouble(),
              discount_price: list_item[i]['discount_price'].toDouble(),
              favorite: list_item[i]['favorite'] == 1 ? true : false,
              image: list_item[i]['image']));
        }
      }
      await Future.delayed(Duration(seconds: 1));

      setState(() {
        _fashionLoad = false;
      });
    }).catchError((error) {
      NetworkHelper.dioError(context: context, error: error);
    });
  }
}
