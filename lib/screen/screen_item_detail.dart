import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:online_shop_ku/model/item_model.dart';
import 'package:online_shop_ku/util/helper.dart';
import 'package:online_shop_ku/util/network.dart';
import 'package:online_shop_ku/util/resources.dart';
import 'package:online_shop_ku/widget/image_raw.dart';
import 'package:online_shop_ku/widget/toast.dart';

class ScreenItemDetail extends StatefulWidget {
  Item item;
  ScreenItemDetail({
    Key key,
    this.item,
  }) : super(key: key);
  @override
  _ScreenItemDetailState createState() => _ScreenItemDetailState();
}

class _ScreenItemDetailState extends State<ScreenItemDetail> {
  List<String> image = [
    "https://pngimg.com/uploads/tshirt/tshirt_PNG5449.png",
    "https://pngimg.com/uploads/tshirt/tshirt_PNG5450.png",
    "https://i.pinimg.com/originals/62/98/b0/6298b026a65cf80bcf9dce061e9b79c9.png",
  ];
  String _size;
  Color _color;
  String _colorText;
  bool _isProcessing = false;
  int _qty = 1;
  Item item;
  @override
  void initState() {
    super.initState();
    item = widget.item;
  }

  @override
  Widget build(BuildContext context) {
    print("Build: ScreenItemDetail");
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarIconBrightness: Brightness.light,
            systemNavigationBarColor: ResColor.primary),
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: ResColor.secondary,
            shadowColor: Colors.transparent,
            leading: Padding(
              padding: EdgeInsets.all(8),
              child: RawMaterialButton(
                elevation: 0,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                fillColor: ResColor.secondary,
                child: Icon(
                  Icons.arrow_back_ios,
                  size: 25.0,
                  color: Colors.black54,
                ),
                padding: EdgeInsets.all(0.0),
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: InkWell(
                  onTap: () async {
                    print("Favorite");
                    await _addFavorite(item: widget.item);
                    if (item.favorite) {
                      ToastHelper.toast(msg: "Item Removed from favorite");
                    } else {
                      ToastHelper.toast(msg: "Item added to favorite");
                    }
                    item.favorite = !item.favorite;
                    setState(() {});
                  },
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8),
                      child: Icon(
                        widget.item.favorite
                            ? CupertinoIcons.heart_fill
                            : CupertinoIcons.heart,
                        color: ResColor.primary,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          body: Material(
            color: ResColor.secondary,
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _carousel(),
                  Padding(padding: EdgeInsets.all(10)),
                  Expanded(
                    child: Material(
                      borderRadius:
                          BorderRadius.vertical(top: Radius.circular(30)),
                      color: ResColor.white,
                      elevation: 10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(25, 25, 25, 10),
                            child: Row(
                              children: [
                                Flexible(
                                  flex: 2,
                                  child: Row(
                                    children: [
                                      Icon(Icons.apps_rounded),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        item.name ?? "",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 18),
                                      )
                                    ],
                                  ),
                                ),
                                Flexible(
                                    flex: 1,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Icon(
                                          Icons.star,
                                          color: Colors.yellowAccent,
                                        ),
                                        Text(
                                            "${item.rating.toStringAsFixed(2)} (${item.rating_count})")
                                      ],
                                    ))
                              ],
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.fromLTRB(25, 10, 25, 0),
                              child: Column(children: [
                                Text(
                                  item.description ?? "",
                                  style: TextStyle(
                                      height: 1.25,
                                      color: Colors.black38,
                                      fontWeight: FontWeight.w500),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                _title(title: "Item Size"),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    _butttonTextSize(
                                        title: "S",
                                        ontap: () {
                                          setState(() {
                                            _size = "S";
                                          });
                                        }),
                                    _butttonTextSize(
                                        title: "M",
                                        ontap: () {
                                          setState(() {
                                            _size = "M";
                                          });
                                        }),
                                    _butttonTextSize(
                                        title: "L",
                                        ontap: () {
                                          setState(() {
                                            _size = "L";
                                          });
                                        }),
                                    _butttonTextSize(
                                        title: "XL",
                                        ontap: () {
                                          setState(() {
                                            _size = "XL";
                                          });
                                        }),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                _title(title: "Item Color"),
                                SizedBox(
                                  height: 10,
                                ),
                                SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  // padding: EdgeInsets.only(left: 10, right: 10),
                                  clipBehavior: Clip.none,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      _buttonColor(
                                          color: Colors.red,
                                          ontap: () {
                                            setState(() {
                                              _color = Colors.red;
                                              _colorText = "red";
                                            });
                                          }),
                                      _buttonColor(
                                          color: Colors.green,
                                          ontap: () {
                                            setState(() {
                                              _color = Colors.green;
                                              _colorText = "green";
                                            });
                                          }),
                                      _buttonColor(
                                          color: Colors.blue,
                                          ontap: () {
                                            setState(() {
                                              _color = Colors.blue;
                                              _colorText = "blue";
                                            });
                                          }),
                                      _buttonColor(
                                          color: Colors.yellow,
                                          ontap: () {
                                            setState(() {
                                              _color = Colors.yellow;
                                              _colorText = "yellow";
                                            });
                                          }),
                                      _buttonColor(
                                          color: Colors.purple,
                                          ontap: () {
                                            setState(() {
                                              _color = Colors.purple;
                                              _colorText = "purple";
                                            });
                                          })
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Flexible(
                                      flex: 1,
                                      child: Row(
                                        children: [
                                          Text(
                                            "Qty",
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.black38),
                                          ),
                                          _buttonText(
                                              title: "-",
                                              ontap: () {
                                                _qty -= 1;
                                                if (_qty < 1) {
                                                  _qty = 1;
                                                }
                                                setState(() {});
                                              }),
                                          Text("${_qty}",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold)),
                                          _buttonText(
                                              title: "+",
                                              ontap: () {
                                                _qty += 1;
                                                setState(() {});
                                              }),
                                        ],
                                      ),
                                    ),
                                    Flexible(
                                      flex: 1,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "Total : ",
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.black38),
                                          ),
                                          Text(
                                            "\$ ${(item.price * _qty).toStringAsFixed(2)}",
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                    width: double.infinity,
                                    child: RaisedButton(
                                        onPressed: submitCart,
                                        disabledColor:
                                            ResColor.primary.withAlpha(127),
                                        disabledTextColor: Colors.white,
                                        color: ResColor.primary,
                                        textColor: Colors.white,
                                        padding: EdgeInsets.all(10),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(30)),
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              if (_isProcessing)
                                                SizedBox(
                                                    width: 20,
                                                    child: AspectRatio(
                                                        aspectRatio: 1,
                                                        child:
                                                            CircularProgressIndicator(
                                                          valueColor:
                                                              AlwaysStoppedAnimation<
                                                                      Color>(
                                                                  Colors.white),
                                                          strokeWidth: 3,
                                                        ))),
                                              if (!_isProcessing)
                                                Icon(Icons.shopping_cart),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text(
                                                "Add to chart",
                                                style: const TextStyle(
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )
                                            ])))
                              ]))
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  Widget _buttonColor(
      {@required Color color, Function ontap, EdgeInsets edge: null}) {
    return RawMaterialButton(
      onPressed: () {
        ToastHelper.toast(msg: "Color selected");
        ontap();
      },
      elevation: 2,
      fillColor: color,
      child: (_color == color)
          ? Center(
              child: Icon(
                Icons.check,
                color: Colors.white,
              ),
            )
          : null,
      shape: CircleBorder(),
    );
  }

  Widget _buttonText({String title, Function ontap}) {
    return InkWell(
      onTap: () {
        ontap();
      },
      splashColor: ResColor.secondary,
      child: Padding(
        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        child: Text(title ?? "",
            style: TextStyle(
                color: Colors.black87,
                fontSize: 18,
                fontWeight: FontWeight.bold)),
      ),
    );
  }

  Widget _butttonTextSize({String title, Function ontap}) {
    return InkWell(
      onTap: () {
        ToastHelper.toast(msg: "Item size selected");
        ontap();
      },
      splashColor: ResColor.secondary,
      child: Padding(
        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
        child: Text(title ?? "",
            style: TextStyle(
                color: (_size == title) ? ResColor.primary : Colors.black87,
                fontSize: 18,
                fontWeight: FontWeight.w600)),
      ),
    );
  }

  Widget _title({String title}) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Text(
        title ?? "",
        style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
      ),
    ]);
  }

  Widget _carousel() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.20,
      child: CarouselSlider.builder(
        itemCount: image.length,
        options: CarouselOptions(
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 10),
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            viewportFraction: 1,
            enableInfiniteScroll: false,
            height: MediaQuery.of(context).size.height * 0.75),
        itemBuilder: (ctx, index, realIdx) => ImageRaw(
          url: image[index],
          imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: imageProvider, fit: BoxFit.scaleDown)),
          ),
        ),
      ),
    );
  }

  Future<void> submitCart() async {
    bool _aman = true;
    if (_colorText == null) {
      ToastHelper.toast(msg: "Pilih Warna untuk checkout");
      _aman = false;
    }
    if (_size == null) {
      ToastHelper.toast(msg: "Pilih Ukuran untuk checkout");
      _aman = false;
    }
    if (_aman) {
      setState(() {
        _isProcessing = true;
      });
      await Dio().post('${Helper.getHostApi()}cart', data: {
        'item_id': item.id,
        'size': _size,
        'color': _colorText,
        'price': item.price,
        'quantity': _qty
      }).then((response) {
        if (response.data['status'] == 'ok') {
          ToastHelper.toast(msg: "Item added to cart");
          setState(() {
            _isProcessing = false;
          });
        }
      }).catchError((error) {
        setState(() {
          _isProcessing = false;
        });
        print(error);
        NetworkHelper.dioError(context: context, error: error);
      });
    }
  }

  Future<void> _addFavorite({Item item}) async {
    print("Wishlist");
    print('${Helper.getHostApi()}item/favorite/${item.id}');
    await Dio()
        .put('${Helper.getHostApi()}item/favorite/${item.id}')
        .then((response) {
      print(response);
      if (response.data['status'] == 'ok') {}
    }).catchError((error) {
      NetworkHelper.dioError(context: context, error: error);
    });
  }
}
