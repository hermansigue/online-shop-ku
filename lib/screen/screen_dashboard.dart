import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:online_shop_ku/main.dart';
import 'package:online_shop_ku/screen/screen_cart.dart';
import 'package:online_shop_ku/screen/screen_dashboard_favorite.dart';
import 'package:online_shop_ku/screen/screen_dashboard_history.dart';
import 'package:online_shop_ku/screen/screen_dashboard_home.dart';
import 'package:online_shop_ku/screen/screen_dashboard_profile.dart';
import 'package:online_shop_ku/util/app.dart';
import 'package:online_shop_ku/util/helper.dart';
import 'package:online_shop_ku/util/resources.dart';
import 'package:online_shop_ku/widget/image_raw.dart';

class ScreenDashboard extends StatefulWidget {
  @override
  _ScreenDashboardState createState() => _ScreenDashboardState();
}

class _ScreenDashboardState extends State<ScreenDashboard> {
  PageController _pageController;
  int _curPage = 0;

  @override
  void initState() {
    super.initState();
    _pageController = new PageController(initialPage: _curPage);
  }

  @override
  Widget build(BuildContext context) {
    print("Build HomePage");
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.light,
          systemNavigationBarColor: ResColor.primary),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ResColor.secondary,
          shadowColor: Colors.transparent,
          leading: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ImageRaw(
              url:
                  "https://scontent.fbdo2-1.fna.fbcdn.net/v/t1.6435-9/126904919_3239103142865754_8396772608162789099_n.jpg?_nc_cat=109&ccb=1-3&_nc_sid=174925&_nc_ohc=embrz9EHJQ0AX8bOM6l&_nc_ht=scontent.fbdo2-1.fna&oh=4e59befcab2cd4eba2829bffbf6a134b&oe=60B50A48",
              imageBuilder: (context, imageProvider) => Container(
                width: 35.0,
                height: 35.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image:
                      DecorationImage(image: imageProvider, fit: BoxFit.cover),
                ),
              ),
            ),
          ),
          actions: [
            InkWell(
              onTap: () async {
                Navigator.of(context)
                    .push(CupertinoPageRoute(builder: (BuildContext context) {
                  return ScreenCart();
                }));
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Stack(
                  children: [
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.all(8),
                        child: Icon(
                          Icons.shopping_cart_outlined,
                          color: Colors.black87,
                        ),
                      ),
                    ),
                    Positioned(
                        top: 10,
                        right: 8,
                        child: ClipOval(
                            child: Container(
                                color: ResColor.primary,
                                width: 15,
                                height: 15,
                                child: Center(
                                    child: Text(
                                  App.getCart().toString(),
                                  style: TextStyle(
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )))))
                  ],
                ),
              ),
            ),
          ],
        ),
        body: PageView(
          controller: _pageController,
          physics: NeverScrollableScrollPhysics(),
          children: [
            ScreenDashboardHome(),
            ScreenDashboardFavorite(),
            ScreenDashboardHistory(),
            ScreenDashboardProfile(),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _curPage,
          onTap: (page) {
            print("Pages :" + page.toString());
            setState(() {
              _curPage = page;
            });

            _pageController.animateToPage(page,
                duration: Duration(milliseconds: ResInt.animDuration),
                curve: Curves.ease);
          },
          backgroundColor: Colors.white,
          unselectedItemColor: Colors.grey[400],
          type: BottomNavigationBarType.fixed,
          selectedLabelStyle: TextStyle(fontSize: 12),
          selectedItemColor: ResColor.primary,
          iconSize: 35,
          items: [
            BottomNavigationBarItem(
                icon: Padding(
                  padding: const EdgeInsets.only(top: 2, bottom: 4),
                  child: Icon(
                    Icons.home,
                    color:
                        (_curPage == 0) ? ResColor.primary : Colors.grey[400],
                  ),
                ),
                label: "Home"),
            BottomNavigationBarItem(
                icon: Padding(
                  padding: const EdgeInsets.only(top: 2, bottom: 4),
                  child: Icon(
                    Icons.favorite,
                    color:
                        (_curPage == 1) ? ResColor.primary : Colors.grey[400],
                  ),
                ),
                label: "Favorit"),
            BottomNavigationBarItem(
                icon: Padding(
                  padding: const EdgeInsets.only(top: 2, bottom: 4),
                  child: Icon(
                    Icons.timelapse_rounded,
                    color:
                        (_curPage == 2) ? ResColor.primary : Colors.grey[400],
                  ),
                ),
                label: "History"),
            BottomNavigationBarItem(
                icon: Padding(
                  padding: const EdgeInsets.only(top: 2, bottom: 4),
                  child: Icon(
                    Icons.person_outline_sharp,
                    color:
                        (_curPage == 3) ? ResColor.primary : Colors.grey[400],
                  ),
                ),
                label: "Profile")
          ],
        ),
      ),
    );
  }
}
