import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:online_shop_ku/util/resources.dart';
import 'package:online_shop_ku/widget/image_raw.dart';

class ScreenDashboardProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.light,
          systemNavigationBarColor: ResColor.primary),
      child: Material(
          color: ResColor.secondary,
          child: Padding(
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                ImageRaw(
                  url:
                      "https://scontent.fbdo2-1.fna.fbcdn.net/v/t1.6435-9/126904919_3239103142865754_8396772608162789099_n.jpg?_nc_cat=109&ccb=1-3&_nc_sid=174925&_nc_ohc=embrz9EHJQ0AX8bOM6l&_nc_ht=scontent.fbdo2-1.fna&oh=4e59befcab2cd4eba2829bffbf6a134b&oe=60B50A48",
                  imageBuilder: (context, imageProvider) => Container(
                    width: 200.0,
                    height: 200.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: imageProvider, fit: BoxFit.cover),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text(
                    "Fachri Hammad FP",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                  ),
                )
              ],
            ),
          )),
    );
  }
}
