import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:online_shop_ku/util/resources.dart';

class ScreenDashboardHistory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.light,
          systemNavigationBarColor: ResColor.primary),
      child: Material(
          color: ResColor.secondary,
          child: Padding(
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Center(
                    child: Text(
                  "This History Page",
                )),
                Center(
                    child: Text(
                  "No Data Display",
                ))
              ],
            ),
          )),
    );
  }
}
