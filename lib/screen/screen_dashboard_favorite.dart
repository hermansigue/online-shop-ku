import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:online_shop_ku/model/item_model.dart';
import 'package:online_shop_ku/screen/screen_item_detail.dart';
import 'package:online_shop_ku/util/helper.dart';
import 'package:online_shop_ku/util/network.dart';
import 'package:online_shop_ku/util/resources.dart';
import 'package:online_shop_ku/widget/item_tile.dart';
import 'package:online_shop_ku/widget/loading.dart';
import 'package:online_shop_ku/widget/toast.dart';

class ScreenDashboardFavorite extends StatefulWidget {
  @override
  _ScreenDashboardFavoriteState createState() =>
      _ScreenDashboardFavoriteState();
}

class _ScreenDashboardFavoriteState extends State<ScreenDashboardFavorite> {
  List<Item> _list_item = [];
  bool _fashionLoad = false;

  @override
  void initState() {
    super.initState();
    _getItem();
  }

  @override
  Widget build(BuildContext context) {
    print("Build: ScreenDashboardFavorite");
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.light,
          systemNavigationBarColor: ResColor.primary),
      child: Material(
        color: ResColor.secondary,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _title(
                    title: "Favorite Fashion",
                  )
                ],
              ),
            ),
            Expanded(
              child: _fashionLoad
                  ? LoadingScreen()
                  : _list_item.length == 0
                      ? Center(
                          child: Text("No data Favourite"),
                        )
                      : GridView.builder(
                          padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            childAspectRatio: 7.0 / 10.0,
                            crossAxisCount: 2,
                          ),
                          itemCount: _list_item.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ItemTile(
                              item: _list_item[index],
                              ontap: () {
                                Navigator.of(context).push(CupertinoPageRoute(
                                    builder: (BuildContext context) {
                                  return ScreenItemDetail(
                                    item: _list_item[index],
                                  );
                                }));
                              },
                              callback: () {
                                _list_item.removeAt(index);
                                setState(() {});
                                ToastHelper.toast(
                                    msg: "Item Removed from favorite");
                              },
                            );
                          }),
            ),
          ],
        ),
      ),
    );
  }

  Widget _title({String title}) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Text(
        title ?? "",
        style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
      ),
    ]);
  }

  Future<void> _getItem() async {
    print("Get Item");
    setState(() {
      _fashionLoad = true;
    });

    await Dio().get('${Helper.getHostApi()}favorite').then((response) async {
      if (response.data['status'] == 'ok') {
        _list_item.clear();
        var list_item = response.data['data'];
        for (var i = 0; i < list_item.length; i++) {
          _list_item.add(Item(
              id: list_item[i]['id'],
              name: list_item[i]['name'],
              description: list_item[i]['description'],
              rating: list_item[i]['rating'].toDouble(),
              rating_count: list_item[i]['rating_count'].toInt(),
              price: list_item[i]['price'].toDouble(),
              discount_price: list_item[i]['discount_price'].toDouble(),
              favorite: list_item[i]['favorite'] == 1 ? true : false,
              image: list_item[i]['image']));
        }
      }
      await Future.delayed(Duration(seconds: 1));

      setState(() {
        _fashionLoad = false;
      });
    }).catchError((error) {
      NetworkHelper.dioError(context: context, error: error);
    });
  }
}
